/**
 * Data Access Objekty zdroju Destination, Flight, Payment a Reservation.
 * Obsahuje taky PersistenceException pro vyjimku z databaze.
 */
package cz.aos.persistence;