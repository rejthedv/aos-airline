package cz.aos.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/** 
 * Obecne DAO pro pristup k databazi.
 * @author rejthedv
 */
public class AbstractDAO {

	protected EntityManagerFactory factory = Persistence.createEntityManagerFactory("persist-unit");

	protected EntityManager getEntityManager() {
		return this.factory.createEntityManager();
	}
}
