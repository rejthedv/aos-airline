/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.aos.entity;

import javax.persistence.*;

/** 
 * Trida popisuje databazovou entitu Destination.
 * @author rejthedv
 */
@Entity
@Table(name = "destination")
public class DestinationEntity extends AbstractEntity {

	@Id
	@Column(name = "id")
	protected Long id;

	@Column(name = "destination_name", nullable = false)
	private String name;

	@Column(name = "latitude", nullable = false)
	private double latitude;

	@Column(name = "longitude", nullable = false)
	private double longitude;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
