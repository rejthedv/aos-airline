/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.aos.api.data;

import java.io.Serializable;
import java.util.Date;

/**
 * Nosic dat zdroje Reservation. Drzi si informace v lokalnich promennych.
 * @author rejthedv
 */
public class Reservation implements Serializable {
    
    public final static String PAID = "PAID";
    public final static String NEW = "NEW";

	private Long id;
	private int flight;
	private int seats;
	private String password;
	private String state;
	private String created;
	private String url;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getFlight() {
		return flight;
	}

	public void setFlight(int flight) {
		this.flight = flight;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
