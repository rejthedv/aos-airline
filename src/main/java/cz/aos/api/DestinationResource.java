/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


package cz.aos.api;

import com.google.gson.*;
import cz.aos.api.data.Destination;
import cz.aos.persistence.PersistenceException;
import cz.aos.service.DestinationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.annotation.security.RolesAllowed;

/**
 * Rozhrani pro praci se zdrojem Destination.
 * @author edvard
 */
@Path(value = "destination")
public class DestinationResource {

    protected DestinationService service;

    public DestinationResource() {
        this.service = new DestinationService();
    }

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAll() {
        System.out.println("get all destinations...");
        ArrayList<Destination> list = this.service.find();
        Response.ResponseBuilder builder = Response.ok(list);
        return builder.build();
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Destination getData(@PathParam("id") Long id) {
        return this.service.find(id);
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin"})
    public Response saveDestination(Destination data) {
        try {

            String url = "http://maps.googleapis.com/maps/api/geocode/json?address=" + data.getName() + "&sensor=false";

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Czech Technical University - AOS bot - Rejthar Edvard");

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            InputStreamReader isr = new InputStreamReader(con.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //zpracovat dosly JSON
            System.out.println("Google API response:");
            System.out.println(response.toString());

            JsonParser parser = new JsonParser();
            System.out.println("Testab");
            JsonObject rootObj = parser.parse(response.toString()).getAsJsonObject();
            System.out.println(rootObj);
            JsonObject locObj = rootObj.getAsJsonArray("results").get(0).getAsJsonObject().getAsJsonObject("geometry").getAsJsonObject("location");
            String status = rootObj.get("status").getAsString();
            String lat = locObj.get("lat").getAsString();
            String lng = locObj.get("lng").getAsString();

            data.setLatitude(Double.parseDouble(lat));
            data.setLongitude(Double.parseDouble(lng));
            this.service.create(data);
            System.out.println(data.getLatitude());
            return Response.status(Response.Status.OK).entity("Destination " + data.getId() + " created.").type(MediaType.APPLICATION_JSON).build();
        } catch (PersistenceException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        } catch (MalformedURLException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        } catch (IOException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @PUT
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateData(@PathParam("id") Long id, Destination data) {
        try {
            this.service.update(id, data);
            return Response.status(Response.Status.OK).entity("Destination " + data.getId() + " updated.").type(MediaType.APPLICATION_JSON).build();
        } catch (PersistenceException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteData(@PathParam("id") Long id) {
        try {
            this.service.delete(id);
            return Response.status(Response.Status.OK).entity("Destination " + id + " deleted.").type(MediaType.APPLICATION_JSON).build();
        } catch (PersistenceException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }
}
