/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.aos.api;

import cz.aos.api.data.Reservation;
import cz.aos.persistence.PersistenceException;
import cz.aos.service.ReservationService;
import cz.aos.service.ServiceException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import javax.annotation.security.RolesAllowed;

/**
 * Rozhrani pro praci se zdrojem Reservation.
 * @author edvard
 */
@Path(value = "reservation")
public class ReservationResource {

    protected ReservationService service;

    public ReservationResource() {
        this.service = new ReservationService();
    }

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAll() { 
        System.out.println("get all reservations...");
        ArrayList<Reservation> list = this.service.find();
        Response.ResponseBuilder builder = Response.ok(list);
        return builder.build();
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Reservation getData(@PathParam("id") Long id, @HeaderParam("x-Password") String password) {
        System.out.println("get single reservation...");
        Reservation res = this.service.find(id);
        if (checkPassword(res, password)) {
            System.out.println("check ok");
            return res;
        } else {
            System.out.println("check failed");
            return null;
        }
    }

    protected boolean checkPassword(Reservation res, String password) {
        return (res.getPassword().equals(password));
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveReservation(Reservation data) {
        try {
            this.service.create(data);
            return Response.status(Response.Status.OK).entity("Reservation " + data.getId() + " created.").type(MediaType.APPLICATION_JSON).build();
        } catch (PersistenceException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        } catch (ServiceException e) {
            return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @PUT
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateData(@PathParam("id") Long id, Reservation data, @HeaderParam("x-Password") String password) {
        if (checkPassword(this.service.find(id), password)) {
            System.out.println("check ok");
            try {
                this.service.update(id, data);
                return Response.status(Response.Status.OK).entity("Reservation " + data.getId() + " updated.").type(MediaType.APPLICATION_JSON).build();
            } catch (PersistenceException e) {
                return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
            } catch (ServiceException e) {
                return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
            }
        } else {
            System.out.println("check failed");
            return null;
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteData(@PathParam("id") Long id) {
        try {
            this.service.delete(id);
            return Response.status(Response.Status.OK).entity("Reservation " + id + " deleted.").type(MediaType.APPLICATION_JSON).build();
        } catch (PersistenceException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }
}
