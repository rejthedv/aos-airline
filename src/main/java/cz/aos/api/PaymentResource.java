/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.aos.api;

import cz.aos.api.data.Reservation;
import cz.aos.persistence.PersistenceException;
import cz.aos.service.ReservationService;
import cz.aos.service.ServiceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.apache.derby.iapi.types.XML;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Rozhrani pro praci se zdrojem Payment.
 * @author edvard
 */
@Path(value = "reservation/{reservation_id}/payment")
public class PaymentResource {

    @GET
    public String get() {
        System.out.println("get payment");
        return "get payment returned";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String post(@PathParam("reservation_id") Long id, JSONObject json) {
        ReservationService service = new ReservationService();
        Reservation r = service.find(id);
        r.setState(Reservation.PAID);

        try {
            service.update(id, r);
            System.out.println(json.getString("account-number") + " - json");
        } catch (JSONException ex) {
            Logger.getLogger(PaymentResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PersistenceException ex) {
            Logger.getLogger(PaymentResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(PaymentResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "prijmuto json JOO" + r + r.getState();
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public String postXml(@PathParam("reservation_id") Long id, Object xml) {
        ReservationService service = new ReservationService();
        Reservation r = service.find(id);
        r.setState(Reservation.PAID);
        System.out.println("xml");
        try {
            service.update(id, r);
        } catch (PersistenceException ex) {
            Logger.getLogger(PaymentResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(PaymentResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "prijmuto xml";
    }
}
