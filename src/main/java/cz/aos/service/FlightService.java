/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.aos.service;

import cz.aos.api.data.Flight;
import cz.aos.entity.FlightEntity;
import cz.aos.persistence.FlightDAO;
import cz.aos.persistence.PersistenceException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Poskytuje objekty Flight.
 * @author rejthedv 27.10.14.
 */
public class FlightService {

	private String dateformat = "yyyy-MM-dd'T'HH:mm:ssXXX";

	private FlightDAO dao;

	public FlightService() {
		dao = new FlightDAO();
	}

	public ArrayList<Flight> find(int offset, int limit, String order, String filter) throws PersistenceException {
		return this.entityListToDataList(dao.getAll(offset, limit, order, filter));
	}

	public Flight find(Long id) {
		return this.entityToData(dao.get(id));
	}

	public void delete(Long id) throws PersistenceException {
		dao.delete(id);
	}

	public void create(Flight data) throws PersistenceException, ServiceException {
		dao.create(this.dataToEntity(data));
	}

	public void update(Long id, Flight data) throws PersistenceException, ServiceException {
		data.setId(id);
		dao.update(this.dataToEntity(data));
	}

	private ArrayList<Flight> entityListToDataList(List<FlightEntity> entityList) {
		ArrayList<Flight> dataList = new ArrayList<Flight>();
		for (FlightEntity entity : entityList) {
			dataList.add(this.entityToData(entity));
		}
		return dataList;
	}

	private ArrayList<FlightEntity> dataListToEntityList(List<Flight> datalist) throws ServiceException {
		ArrayList<FlightEntity> entityList = new ArrayList<FlightEntity>();
		for (Flight data : datalist) {
			entityList.add(this.dataToEntity(data));
		}
		return entityList;
	}

	private Flight entityToData(FlightEntity entity) {
            System.out.println("edvard3");
		SimpleDateFormat df = new SimpleDateFormat(this.dateformat);
		Flight data = new Flight();
		if (entity != null) {
			data.setId(entity.getId());
			data.setName(entity.getName());
			data.setDistance(entity.getDistance());
			data.setDateOfDeparture(df.format(entity.getDateOfDeparture()));
			data.setSeats(entity.getSeats());
			data.setTo(entity.getTo());
			data.setFrom(entity.getFrom());
			data.setPrice(entity.getPrice());
			data.setUrl("/flight/" + entity.getId());
		}                
		return data;
	}

	private FlightEntity dataToEntity(Flight data) throws ServiceException {
		SimpleDateFormat df = new SimpleDateFormat(this.dateformat);
		FlightEntity entity = new FlightEntity();
		try {
			if (data != null) {
				entity.setId(data.getId());
				entity.setName(data.getName());
				entity.setDistance(data.getDistance());
				entity.setDateOfDeparture(df.parse(data.getDateOfDeparture()));
				entity.setSeats(data.getSeats());
				entity.setTo(data.getTo());
				entity.setFrom(data.getFrom());
				entity.setPrice(data.getPrice());
			}
		} catch (ParseException e) {
			throw new ServiceException("Bad dateformat: " + this.dateformat + " " + data.getDateOfDeparture());
		}
		return entity;
	}
}
