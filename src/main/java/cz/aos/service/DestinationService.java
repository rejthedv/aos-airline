/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.aos.service;

import cz.aos.api.data.Destination;
import cz.aos.entity.DestinationEntity;
import cz.aos.persistence.DestinationDAO;
import cz.aos.persistence.PersistenceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Poskytuje objekty Destination.
 * @author rejthedv 27.10.14.
 */
public class DestinationService {

	private DestinationDAO dao;

	public DestinationService() {
		dao = new DestinationDAO();
	}

	public ArrayList<Destination> find() {
		return this.entityListToDataList(dao.getAll());
	}

	public Destination find(Long id) {
		return this.entityToData(dao.get(id));
	}

	public void delete(Long id) throws PersistenceException {
		dao.delete(id);
	}

	public void create(Destination data) throws PersistenceException {
		dao.create(this.dataToEntity(data));
	}

	public void update(Long id, Destination data) throws PersistenceException {
		data.setId(id);
		dao.update(this.dataToEntity(data));
	}

	private ArrayList<Destination> entityListToDataList(List<DestinationEntity> entityList) {
		ArrayList<Destination> dataList = new ArrayList<Destination>();
		for (DestinationEntity entity : entityList) {
			dataList.add(this.entityToData(entity));
		}
		return dataList;
	}

	private ArrayList<DestinationEntity> dataListToEntityList(List<Destination> datalist) {
		ArrayList<DestinationEntity> entityList = new ArrayList<DestinationEntity>();
		for (Destination data : datalist) {
			entityList.add(this.dataToEntity(data));
		}
		return entityList;
	}

	private Destination entityToData(DestinationEntity entity) {
		Destination data = new Destination();
		if (entity != null) {
			data.setId(entity.getId());
			data.setName(entity.getName());
			data.setLatitude(entity.getLatitude());
			data.setLongitude(entity.getLongitude());
			data.setUrl("/destination/" + entity.getId());
		}
		return data;
	}

	private DestinationEntity dataToEntity(Destination data) {
		DestinationEntity entity = new DestinationEntity();
		if (data != null) {
			entity.setId(data.getId());
			entity.setName(data.getName());
			entity.setLatitude(data.getLatitude());
			entity.setLongitude(data.getLongitude());
		}
		return entity;
	}
}
