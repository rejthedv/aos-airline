/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.aos.service;

import java.io.IOException;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.OnClose;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * Endpoint WebSocketu, ktery hlida pocet prihlasenych uzivatelu.
 * @author edvard
 */
@ServerEndpoint("/endpoint")
public class ClientCountWebSocketEndpoint {

    static int count = 0;

    @OnMessage
    public String onMessage(String message) {
        //System.out.println("Server prijima zpravu: " + message);
        if (message.equals("count")) {
            return count + "";
        } else {
            return "Zprava prijata: " + message;
        }
    }

    /**
     * Otevreni noveho websocket spojeni.
     *
     * @param session
     * @throws IOException
     */
    @OnOpen
    public void onOpen(Session session) throws IOException {
        System.out.println("Novy uzivatel");
        count++;
        session.getBasicRemote().sendText(count+"");
    }

    @OnClose
    public void onClose(Session s) {
        count--;
        System.out.println("WebSocket session ends");
    }

}
