/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tickets;

import cz.aos.api.data.Reservation;
import cz.aos.service.ReservationService;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 * Webova sluzba, ktera se stara o tisk letenky z rezervace. Rezervace musi byt ve stavu PAID.
 * @author rejthedv
 */
@WebService(serviceName = "Tickets")
public class Tickets {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        try {
            System.out.println("WEB service jede!met");
            String str = "This is a String ~ GoGoGo";
            InputStream is = new ByteArrayInputStream(str.getBytes());
        } catch (Exception ex) {
            Logger.getLogger(Tickets.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getFile")
    public String getFile(@WebParam(name = "reservation") String id) {
        System.out.println("GET FILE BEGIN");
        ReservationService service = new ReservationService();
        Reservation res = service.find(Long.parseLong(id));
        System.out.println("RES");
        if ("PAID".equals(res.getState())) {
            System.out.println("PAID");
            return "letenka file" + id;
        }
        System.out.println("not in paid state");
        return "not in paid state";
    }


    /**
     * Web service operation
     */
    @WebMethod(operationName = "getFileFromReservationJson")
    public String getFileFromReservationJson(@WebParam(name = "reservation") Reservation reservation) {
        if ("PAID".equals(reservation.getState())) {
            System.out.println("PAID");
            return "letenka file" + reservation.getId();
        }
        System.out.println("not in paid state");
        return "not in paid state";
    }

}
