/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package printer;

import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

/**
 * JMS Consumer sluzby Printer. Posloucha na kanale, prijima generovanou zpravu od JMS Producera a zasle letenku na e-mail.
 * Pro jednoduchost v ramci projektu e-mail neposle, ale pouze vypise do informaci do konzole.
 * @author rejthedv
 */
/*
 @MessageDriven(name = "MessageConsumer", activationConfig = {
 @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
 @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/Topic"),
 @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
 })*/
public class PrinterConsumer implements MessageListener {

    @Resource(lookup = "jms/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(lookup = "jms/Topic")
    private Topic topic;

    private final static Logger LOGGER = Logger.getLogger(PrinterConsumer.class.toString());

    public void onMessage(Message rcvMessage) {
        System.out.println("ON MESSAGE!!!! JOOOOOOOOOOOOOOOOOOOOOOOOO!!!!!!!!!!!!§");

        Destination dest = topic;

        Connection connection = null;
        try {
            Destination destination = topic;
            System.out.println("creating connection");
            connection = connectionFactory.createConnection();
            System.out.println("creating session");
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            System.out.println("creating producer");
            MessageConsumer consumer = session.createConsumer(dest);

            // settings                
            connection.start();

            while (true) {
                Message m = consumer.receive(1);
                if (m != null) {
                    if (m instanceof TextMessage) {
                        ObjectMessage message = (ObjectMessage) m;
                        MessageWrapper wr = (MessageWrapper) message.getObject();
                        System.out.println("Odeslu e-mail: " + wr.getEmail() + " o rezervaci: " + wr.getReservation().toString());
                    } else {
                        break;
                    }
                }
            }

        } catch (JMSException e) {
            System.out.println("JMSE >> " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    System.out.println("JMSE 2 >> " + e.getMessage());
                }
            }
        }
        System.out.println("konec producera");

    }
}
