/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package printer;

import cz.aos.api.data.Reservation;
import cz.aos.service.ReservationService;

import javax.annotation.Resource;
import javax.jms.*;
import javax.jws.WebService;

/**
 * Sluzba Printer slouzi k zaslani letenky na e-mail uzivatele.
 * @author rejthedv
 */
@WebService(serviceName = "Printer")
public class Printer {

    @Resource(lookup = "jms/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(lookup = "jms/Topic")
    private Topic topic;

    /**
     * Metoda iniciuje poslani letenky na dany e-mail.
     * @param email E-mail, na ktery se ma letenka poslat.
     * @param reservation ID rezervace.
     */
    public void sendTicket(java.lang.String email, long reservation) {      
        System.out.println("Reservation with ID " + reservation + " will be send to email " + email);

        Connection connection = null;
        try {
            Destination destination = topic;
            System.out.println("creating connection");
            connection = connectionFactory.createConnection();
            System.out.println("creating session");
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            System.out.println("creating producer");
            MessageProducer messageProducer = session.createProducer(destination);

            // settings                
            messageProducer.setTimeToLive(10000);
            messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);
            connection.start();

            // send object message
            ReservationService service = new ReservationService();
            Reservation res = service.find(Long.parseLong(reservation + ""));

            ObjectMessage objectMessage = session.createObjectMessage();
            objectMessage.setObject(new MessageWrapper(res, email));
            System.out.println("sending message from producer");
            messageProducer.send(objectMessage);
            System.out.println("message sent");

            MessageWrapper wr = (MessageWrapper) objectMessage.getObject();
            System.out.println("Odeslu e-mail: " + wr.getEmail() + " o rezervaci: " + wr.getReservation().getId());                        
            
        } catch (JMSException e) {
            System.out.println("JMSE >> " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    System.out.println("JMSE 2 >> " + e.getMessage());                    
                }
            }
        }
        System.out.println("konec producera");
    }

}
