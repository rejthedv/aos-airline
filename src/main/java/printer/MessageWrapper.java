/*
 * Copyright (c) 2014 Edvard Rejthar <cvut@edvard.cz>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package printer;

import cz.aos.api.data.Reservation;
import java.io.Serializable;

/**
 * Trida slouzi pro predavani zpravy o letence skrz JMS.
 * @author rejthedv
 */
public class MessageWrapper implements Serializable {

    private Reservation reservation;
    private String email;

    public MessageWrapper(Reservation reservation, String email) {
        this.reservation = reservation;
        this.email = email;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public String getEmail() {
        return email;
    }
    
    

    @Override
    public String toString() {
        return "ObjectMessage{reservation='" + reservation.toString() + "', email='" + email + "'}";
    }
}
