pickerformat = "Y-m-d H:i:s";//-MM-DDThh:mm:ssTZD
headers = {};

//
//pickerformat = "'T'";//-MM-DDThh:mm:ssTZD
var Client, DestinationForm, PaymentForm, DestinationList, FlightForm, FlightList, ReservationForm, ReservationList, client,
        __bind = function (fn, me) {
            return function () {
                return fn.apply(me, arguments);
            };
        };

Client = (function () {
    function Client(container, content) {
        this.container = container;
        this.content = content;
        this.showReservationsList = __bind(this.showReservationsList, this);
        this.showReservationForm = __bind(this.showReservationForm, this);
        this.showDestinationsList = __bind(this.showDestinationsList, this);
        this.showDestinationForm = __bind(this.showDestinationForm, this);
        this.showPaymentForm = __bind(this.showPaymentForm, this);
        this.showFlightsList = __bind(this.showFlightsList, this);
        this.showFlightForm = __bind(this.showFlightForm, this);
        this.setUpListeners = __bind(this.setUpListeners, this);

    }


    Client.prototype.run = function () {
        OnlineClients().start();
        return this.setUpListeners();
    };

    Client.prototype.setUpListeners = function () {
        document.getElementById("createFlight").addEventListener('click', this.showFlightForm, false);
        document.getElementById("listFlights").addEventListener('click', this.showFlightsList, false);
        document.getElementById("makePayment").addEventListener('click', this.showPaymentForm, false);
        document.getElementById("createDestination").addEventListener('click', this.showDestinationForm, false);
        document.getElementById("listDestinations").addEventListener('click', this.showDestinationsList, false);
        document.getElementById("createReservation").addEventListener('click', this.showReservationForm, false);
        return document.getElementById("listReservations").addEventListener('click', this.showReservationsList, false);
    };

    Client.prototype.showFlightForm = function (e) {
        var form;
        return form = new FlightForm(this.content);
    };

    Client.prototype.showFlightsList = function (e) {
        var list;
        return list = new FlightList(this.content);
    };

    Client.prototype.showDestinationForm = function (e) {
        var form;
        return form = new DestinationForm(this.content);
    };

    Client.prototype.showPaymentForm = function (e) {
        var form;
        return form = new PaymentForm(this.content);
    };

    Client.prototype.showDestinationsList = function (e) {
        var list;
        return list = new DestinationList(this.content);
    };

    Client.prototype.showReservationForm = function (e) {
        var form;
        return form = new ReservationForm(this.content);
    };

    Client.prototype.showReservationsList = function (e) {
        var list;
        return list = new ReservationList(this.content);
    };

    return Client;

})();

FlightForm = (function () {
    function FlightForm(content) {
        this.content = content;
        this.sendForm = __bind(this.sendForm, this);
        this.createForm = __bind(this.createForm, this);
        $(this.content).empty();
        this.createForm();
        $('form').submit(this.sendForm);
    }

    FlightForm.prototype.createForm = function () {
        var form, table, tbody;
        form = document.createElement("form");
        form.method = "post";
        form.action = "#";
        table = document.createElement("table");
        tbody = document.createElement("tbody");
        tbody.innerHTML = this.getForm();
        table.appendChild(tbody);
        form.appendChild(table);
        this.content.appendChild(form);
//$('#dateOfDeparture').datetimepicker({format:pickerformat});
        return form;
    };

    FlightForm.prototype.sendForm = function (e) {
        var ar;
        e.preventDefault();
        ar = $(e.target).serializeArray();
        this.data = {};
        this.data[ar[0].name] = ar[0].value;
        this.data[ar[1].name] = ar[1].value;
        this.data[ar[2].name] = Number(ar[2].value);
        this.data[ar[3].name] = Number(ar[3].value);
        this.data[ar[4].name] = Number(ar[4].value);
        this.data[ar[5].name] = Number(ar[5].value);
        this.data[ar[6].name] = Number(ar[6].value);
        this.data["url"] = "";
        $.ajax({
            url: 'http://localhost:8080/airline/source/flight',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data),
        });
        return $(this.content).empty();
    };

    FlightForm.prototype.getForm = function () {
        return "<tr> <th>Name? </th> <td><input type=\"text\" name=\"name\" id=\"name\"></td> </tr> <tr> <th>Date of departure: </th> <td><input type=\"text\" name=\"dateOfDeparture\" id=\"dateOfDeparture\"></td> </tr> <tr> <th>Distance: </th> <td><input type=\"text\" name=\"distance\" id=\"distance\"></td> </tr> <tr> <th>Seats: </th> <td><input type=\"number\" name=\"seats\" id=\"seats\"></td> </tr> <tr> <th>Price: </th> <td><input type=\"text\" name=\"price\" id=\"price\"></td> </tr> <tr> <th>From destination: </th> <td><input type=\"text\" name=\"from\" id=\"from\"></td> </tr> <tr> <th>To destination: </th> <td><input type=\"text\" name=\"to\" id=\"to\"></td> </tr> <tr> <th></th> <td><input type=\"submit\" value=\"Create\"></td> </tr>";
    };

    return FlightForm;

})();

PaymentForm = (function () {
    function PaymentForm(content) {
        this.content = content;
        this.sendForm = __bind(this.sendForm, this);
        this.createForm = __bind(this.createForm, this);
        $(this.content).empty();
        this.createForm();
        $('form').submit(this.sendForm);
    }

    PaymentForm.prototype.createForm = function () {
        var form, table, tbody;
        form = document.createElement("form");
        form.method = "post";
        form.action = "#";
        table = document.createElement("table");
        tbody = document.createElement("tbody");
        tbody.innerHTML = this.getForm();
        table.appendChild(tbody);
        form.appendChild(table);
        this.content.appendChild(form);
        return form;
    };

    PaymentForm.prototype.sendForm = function (e) {
        var ar;
        e.preventDefault();
        ar = $(e.target).serializeArray();
        this.data = {};
        this.data[ar[0].name] = ar[0].value;
        this.data["url"] = "";
        $.ajax({
            url: 'http://localhost:8080/airline/source/reservation/' + Number(ar[1].value) + '/payment',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data)
        });
        return $(this.content).empty();
    };

    PaymentForm.prototype.getForm = function () {
        return "<tr> <th>Account-number: </th> <td><input type=\"text\" name=\"account-number\" id=\"account-number\"></td> </tr> <tr> <th>Reservation id: </th> <td><input type=\"text\" name=\"reservation-id\" id=\"reservation-id\"></td> </tr>  <tr> <th></th> <td><input type=\"submit\" value=\"Pay\"></td> </tr>";
    };

    return PaymentForm;

})();

DestinationForm = (function () {
    function DestinationForm(content) {
        this.content = content;
        this.sendForm = __bind(this.sendForm, this);
        this.createForm = __bind(this.createForm, this);
        $(this.content).empty();
        this.createForm();
        $('form').submit(this.sendForm);
    }

    DestinationForm.prototype.createForm = function () {
        var form, table, tbody;
        form = document.createElement("form");
        form.method = "post";
        form.action = "#";
        table = document.createElement("table");
        tbody = document.createElement("tbody");
        tbody.innerHTML = this.getForm();
        table.appendChild(tbody);
        form.appendChild(table);
        this.content.appendChild(form);
        return form;
    };

    DestinationForm.prototype.sendForm = function (e) {
        var ar;
        e.preventDefault();
        ar = $(e.target).serializeArray();
        this.data = {};
        this.data[ar[0].name] = ar[0].value;
        this.data[ar[1].name] = Number(ar[1].value);
        this.data[ar[2].name] = Number(ar[2].value);
        this.data["url"] = "";
        $.ajax({
            url: 'http://localhost:8080/airline/source/destination',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data)
        });
        return $(this.content).empty();
    };

    DestinationForm.prototype.getForm = function () {
        return "<tr> <th>Name: </th> <td><input type=\"text\" name=\"name\" id=\"name\"></td> </tr> <tr> <th>Latitude: </th> <td><input type=\"text\" name=\"latitude\" id=\"latitude\"></td> </tr> <tr> <th>Longitude: </th> <td><input type=\"text\" name=\"longitude\" id=\"longitude\"></td> </tr> <tr> <th></th> <td><input type=\"submit\" value=\"Create\"></td> </tr>";
    };

    return DestinationForm;

})();

ReservationForm = (function () {
    function ReservationForm(content) {
        this.content = content;
        this.sendForm = __bind(this.sendForm, this);
        this.createForm = __bind(this.createForm, this);
        $(this.content).empty();
        this.createForm();
        $('form').submit(this.sendForm);
    }

    ReservationForm.prototype.createForm = function () {
        var form, table, tbody;
        form = document.createElement("form");
        form.method = "post";
        form.action = "#";
        table = document.createElement("table");
        tbody = document.createElement("tbody");
        tbody.innerHTML = this.getForm();
        table.appendChild(tbody);
        form.appendChild(table);
        this.content.appendChild(form);
        return form;
    };

    ReservationForm.prototype.sendForm = function (e) {
        var ar;
        e.preventDefault();
        ar = $(e.target).serializeArray();
        this.data = {};
        this.data[ar[0].name] = ar[0].value;
        this.data[ar[1].name] = ar[1].value;
        this.data[ar[2].name] = ar[2].value;
        this.data[ar[3].name] = Number(ar[3].value);
        this.data["url"] = "";
        this.data["state"] = "NEW";
        $.ajax({
            url: 'http://localhost:8080/airline/source/reservation',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data)
        });
        return $(this.content).empty();
    };

    ReservationForm.prototype.getForm = function () {
        return "<tr> <th>Flight: </th> <td><input type=\"text\" name=\"flight\" id=\"flight\"></td> </tr> <tr> <th>Created: </th> <td><input type=\"text\" name=\"created\" id=\"created\"></td> </tr> <tr> <th>Password: </th> <td><input type=\"text\" name=\"password\" id=\"password\"></td> </tr> <tr> <th>Seats: </th> <td><input type=\"number\" name=\"seats\" id=\"seats\"></td> </tr> <tr> <th></th> <td><input type=\"submit\" value=\"Create\"></td> </tr>";
    };

    return ReservationForm;

})();

FlightList = (function () {
    function FlightList(content) {
        this.content = content;
        this.sendForm = __bind(this.sendForm, this);
        this.createForm = __bind(this.createForm, this);
        this.updateData = __bind(this.updateData, this);
        this.deleteData = __bind(this.deleteData, this);
        this.printList = __bind(this.printList, this);
        $(this.content).empty();

        $.ajax({
            url: 'http://localhost:8080/airline/source/flight',
            type: 'GET',
            dataType: 'json',
            headers: headers

        }).done(this.printList);
    }

    FlightList.prototype.printList = function (data, status, request) {
        var flight, row, table, thead, _i, _len, _results;
        table = document.createElement("table");
        thead = document.createElement("thead");
        row = document.createElement("tr");
        row.innerHTML = "<th>ID</th>" +
                "<th>Name<span data-header='X-Order' data-value='name:desc'>▲</span> <span data-header='X-Order' data-value='name:asc'>▼</span></th>" +
                "<th>Date of departure<span data-header='X-Order' data-value='dateOfDeparture:desc'>▲</span> <span data-header='X-Order' data-value='dateOfDeparture:asc'>▼</span></th>" +
                "<th>Distance<span data-header='X-Order' data-value='distance:desc'>▲</span> <span data-header='X-Order' data-value='distance:asc'>▼</span></th>" +
                "<th>Seats<span data-header='X-Order' data-value='seats:desc'>▲</span> <span data-header='X-Order' data-value='seats:asc'>▼</span></th>" +
                "<th>Price<span data-header='X-Order' data-value='price:desc'>▲</span> <span data-header='X-Order' data-value='price:asc'>▼</span></th>" +
                "<th>From<span data-header='X-Order' data-value='from:desc'>▲</span> <span data-header='X-Order' data-value='from:asc'>▼</span></th>" +
                "<th>To<span data-header='X-Order' data-value='to:desc'>▲</span> <span data-header='X-Order' data-value='to:asc'>▼</span></th>" +
                "<th>URL</th>" + "<th>Delete</th>";
        thead.appendChild(row);
        table.appendChild(thead);
        this.tbody = document.createElement("tbody");
        table.appendChild(this.tbody);
        this.content.appendChild(table);
        //filter, paging
        $("<input />", {placeholder: "X-Filter", value: headers["X-Filter"]}).insertAfter($("table")).change(function () {
            headers["X-Filter"] = $(this).val();
            client.showFlightsList();
        });
        $("<input />", {placeholder: "X-Offset", value: headers["X-Offset"]}).insertAfter($("table")).change(function () {
            headers["X-Offset"] = $(this).val();
            client.showFlightsList();
        });
        $("<input />", {placeholder: "X-Base", value: headers["X-Base"]}).insertAfter($("table")).change(function () {
            headers["X-Base"] = $(this).val();
            client.showFlightsList();
        });

        //razeni       
        $("th span").click(function () {
            headers["X-Order"] = $(this).attr("data-value");
            client.showFlightsList();
        });
        _results = [];
        for (_i = 0, _len = data.length; _i < _len; _i++) {
            flight = data[_i];
            _results.push((function (_this) {
                return function (flight) {
                    row = document.createElement("tr");
                    row.innerHTML = "<td>" + flight.id + "</td>" + "<td>" + flight.name + "</td>" + "<td>" + flight.dateOfDeparture + "</td>" + "<td>" + flight.distance + "</td>" + "<td>" + flight.seats + "</td>" + "<td>" + flight.price + "</td>" + "<td>" + flight.from + "</td>" + "<td>" + flight.to + "</td>" + "<td><a id=\"update" + flight.id + "\" data-flight-id=\"" + flight.id + "\" href=\"#\">" + flight.url + "</a></td>" + "<td><a id=\"delete" + flight.id + "\" data-flight-id=\"" + flight.id + "\" href=\"#\" data-action=\"delete\">x</a></td>";
                    _this.tbody.appendChild(row);
                    document.getElementById("update" + flight.id).addEventListener('click', _this.updateData, false);
                    return document.getElementById("delete" + flight.id).addEventListener('click', _this.deleteData, false);
                };
            })(this)(flight));
        }
        return _results;
    };

    FlightList.prototype.deleteData = function (e) {
        if (!confirm("Delete?")) {
            return false;
        }
        var id;
        id = document.getElementById(e.target.id).getAttribute("data-flight-id");
        $.ajax({
            url: 'http://localhost:8080/airline/source/flight/' + id,
            type: 'DELETE',
            dataType: 'json',
            contentType: 'application/json'
        });
        setTimeout((function () {
        }), 1000);
        $(this.content).empty();
        return $.ajax({
            url: 'http://localhost:8080/airline/source/flight',
            type: 'GET',
            dataType: 'json'
        }).done(this.printList);
    };

    FlightList.prototype.updateData = function (e) {
        this.id = document.getElementById(e.target.id).getAttribute("data-flight-id");
        $(this.content).empty();
        return $.ajax({
            url: 'http://localhost:8080/airline/source/flight/' + this.id,
            type: 'GET',
            dataType: 'json'
        }).done(this.createForm);
    };

    FlightList.prototype.createForm = function (data) {
        var form, table, tbody;
        form = document.createElement("form");
        form.method = "post";
        form.action = "#";
        table = document.createElement("table");
        tbody = document.createElement("tbody");
        tbody.innerHTML = this.getForm(data);
        table.appendChild(tbody);
        form.appendChild(table);
        this.content.appendChild(form);
//$('#dateOfDeparture').datetimepicker({format:pickerformat});
        return $('form').submit(this.sendForm);
    };

    FlightList.prototype.sendForm = function (e) {
        var ar;
        e.preventDefault();
        ar = $(e.target).serializeArray();
        this.data = {};
        this.data[ar[0].name] = ar[0].value;
        this.data[ar[1].name] = ar[1].value;
        this.data[ar[2].name] = Number(ar[2].value);
        this.data[ar[3].name] = Number(ar[3].value);
        this.data[ar[4].name] = Number(ar[4].value);
        this.data[ar[5].name] = Number(ar[5].value);
        this.data[ar[6].name] = Number(ar[6].value);
        this.data["url"] = "";
        $.ajax({
            url: 'http://localhost:8080/airline/source/flight/' + this.id,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data)
        });
        return $(this.content).empty();
    };

    FlightList.prototype.getForm = function (data) {
        return "<tr> <th>AName: </th> <td><input type=\"text\" name=\"name\" id=\"name\" value=\"" + data.name + "\"></td> </tr> <tr> <th>Date of departure: </th> <td><input type=\"text\" name=\"dateOfDeparture\" id=\"dateOfDeparture\" value=\"" + data.dateOfDeparture + "\"></td> </tr> <tr> <th>Distance: </th> <td><input type=\"text\" name=\"distance\" id=\"distance\" value=\"" + data.distance + "\"></td> </tr> <tr> <th>Seats: </th> <td><input type=\"number\" name=\"seats\" id=\"seats\" value=\"" + data.seats + "\"></td> </tr> <tr> <th>Price: </th> <td><input type=\"text\" name=\"price\" id=\"price\" value=\"" + data.price + "\"></td> </tr> <tr> <th>From destination: </th> <td><input type=\"text\" name=\"from\" id=\"from\" value=\"" + data.from + "\"></td> </tr> <tr> <th>To destination: </th> <td><input type=\"text\" name=\"to\" id=\"to\" value=\"" + data.to + "\"></td> </tr> <tr> <th></th> <td><input type=\"submit\" value=\"Update\"></td> </tr>";
    };

    return FlightList;

})();

DestinationList = (function () {
    function DestinationList(content) {
        this.content = content;
        this.sendForm = __bind(this.sendForm, this);
        this.createForm = __bind(this.createForm, this);
        this.updateData = __bind(this.updateData, this);
        this.deleteData = __bind(this.deleteData, this);
        this.printList = __bind(this.printList, this);
        $(this.content).empty();
        $.ajax({
            url: 'http://localhost:8080/airline/source/destination',
            type: 'GET',
            dataType: 'json'
        }).done(this.printList);
    }

    DestinationList.prototype.printList = function (data, status, request) {
        var destination, row, table, thead, _i, _len, _results;
        table = document.createElement("table");
        thead = document.createElement("thead");
        row = document.createElement("tr");
        row.innerHTML = "<th>ID</th>" + "<th>Name</th>" + "<th>Latitude</th>" + "<th>Longitude</th>" + "<th>URL</th>" + "<th>Delete</th>";
        thead.appendChild(row);
        table.appendChild(thead);
        this.tbody = document.createElement("tbody");
        table.appendChild(this.tbody);
        this.content.appendChild(table);
        _results = [];
        for (_i = 0, _len = data.length; _i < _len; _i++) {
            destination = data[_i];
            _results.push((function (_this) {
                return function (destination) {
                    row = document.createElement("tr");
                    row.innerHTML = "<td>" + destination.id + "</td>" + "<td>" + destination.name + "</td>" + "<td>" + destination.latitude + "</td>" + "<td>" + destination.longitude + "</td>" + "<td><a id=\"update" + destination.id + "\" data-destination-id=\"" + destination.id + "\" href=\"#\">" + destination.url + "</a></td>" + "<td><a id=\"delete" + destination.id + "\" data-destination-id=\"" + destination.id + "\" href=\"#\"   data-action=\"delete\">x</a></td>";
                    _this.tbody.appendChild(row);
                    document.getElementById("update" + destination.id).addEventListener('click', _this.updateData, false);
                    return document.getElementById("delete" + destination.id).addEventListener('click', _this.deleteData, false);
                };
            })(this)(destination));
        }
        return _results;
    };

    DestinationList.prototype.deleteData = function (e) {
        if (!confirm("Delete?")) {
            return false;
        }
        var id;
        id = document.getElementById(e.target.id).getAttribute("data-destination-id");
        $.ajax({
            url: 'http://localhost:8080/airline/source/destination/' + id,
            type: 'DELETE',
            dataType: 'json',
            contentType: 'application/json'
        });
        setTimeout((function () {
        }), 1000);
        $(this.content).empty();
        return $.ajax({
            url: 'http://localhost:8080/airline/source/destination',
            type: 'GET',
            dataType: 'json'
        }).done(this.printList);
    };

    DestinationList.prototype.updateData = function (e) {
        this.id = document.getElementById(e.target.id).getAttribute("data-destination-id");
        $(this.content).empty();
        return $.ajax({
            url: 'http://localhost:8080/airline/source/destination/' + this.id,
            type: 'GET',
            dataType: 'json'
        }).done(this.createForm);
    };

    DestinationList.prototype.createForm = function (data) {
        var form, table, tbody;
        form = document.createElement("form");
        form.method = "post";
        form.action = "#";
        table = document.createElement("table");
        tbody = document.createElement("tbody");
        tbody.innerHTML = this.getForm(data);
        table.appendChild(tbody);
        form.appendChild(table);
        this.content.appendChild(form);
        //$('#dateOfDeparture').datetimepicker({format:pickerformat});
        return $('form').submit(this.sendForm);
    };

    DestinationList.prototype.sendForm = function (e) {
        var ar;
        e.preventDefault();
        ar = $(e.target).serializeArray();
        this.data = {};
        this.data[ar[0].name] = ar[0].value;
        this.data[ar[1].name] = Number(ar[1].value);
        this.data[ar[2].name] = Number(ar[2].value);
        this.data["url"] = "";
        $.ajax({
            url: 'http://localhost:8080/airline/source/destination/' + this.id,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data)
        });
        return $(this.content).empty();
    };

    DestinationList.prototype.getForm = function (data) {
        return "<tr> <th>Name: </th> <td><input type=\"text\" name=\"name\" id=\"name\" value=\"" + data.name + "\"></td> </tr> <tr> <th>Latitude: </th> <td><input type=\"text\" name=\"latitude\" id=\"latitude\" value=\"" + data.latitude + "\"></td> </tr> <tr> <th>Longitude: </th> <td><input type=\"text\" name=\"longitude\" id=\"longitude\" value=\"" + data.longitude + "\"></td> </tr> <tr> <th></th> <td><input type=\"submit\" value=\"Update\"></td> </tr>";
    };

    return DestinationList;

})();

ReservationList = (function () {
    function ReservationList(content) {
        this.content = content;
        this.sendForm = __bind(this.sendForm, this);
        this.createForm = __bind(this.createForm, this);
        this.updateData = __bind(this.updateData, this);
        this.deleteData = __bind(this.deleteData, this);
        this.printList = __bind(this.printList, this);
        $(this.content).empty();
        $.ajax({
            url: 'http://localhost:8080/airline/source/reservation',
            type: 'GET',
            dataType: 'json'
        }).done(this.printList);
    }

    ReservationList.prototype.printList = function (data, status, request) {
        var reservation, row, table, thead, _i, _len, _results;
        table = document.createElement("table");
        thead = document.createElement("thead");
        row = document.createElement("tr");
        row.innerHTML = "<th>ID</th>" + "<th>Flight</th>" + "<th>Created</th>" + "<th>Password</th>" + "<th>Seats</th>" + "<th>State</th>" + "<th>URL</th>" + "<th>Delete</th>";
        thead.appendChild(row);
        table.appendChild(thead);
        this.tbody = document.createElement("tbody");
        table.appendChild(this.tbody);
        this.content.appendChild(table);
        _results = [];
        for (_i = 0, _len = data.length; _i < _len; _i++) {
            reservation = data[_i];
            _results.push((function (_this) {
                return function (reservation) {
                    row = document.createElement("tr");
                    row.innerHTML = "<td>" + reservation.id + "</td>" + "<td>" + reservation.flight + "</td>" + "<td>" + reservation.created + "</td>" + "<td>" + reservation.password + "</td>" + "<td>" + reservation.seats + "</td>" + "<td>" + reservation.state + "</td>" + "<td><a id=\"update" + reservation.id + "\" data-reservation-id=\"" + reservation.id + "\" href=\"#\">" + reservation.url + "</a></td>" + "<td><a id=\"delete" + reservation.id + "\" data-reservation-id=\"" + reservation.id + "\" href=\"#\"  data-action=\"delete\">x</a></td>";
                    _this.tbody.appendChild(row);
                    document.getElementById("update" + reservation.id).addEventListener('click', _this.updateData, false);
                    return document.getElementById("delete" + reservation.id).addEventListener('click', _this.deleteData, false);
                };
            })(this)(reservation));
        }
        return _results;
    };

    ReservationList.prototype.deleteData = function (e) {
        if (!confirm("Delete?")) {
            return false;
        }
        var id;
        id = document.getElementById(e.target.id).getAttribute("data-reservation-id");
        $.ajax({
            url: 'http://localhost:8080/airline/source/reservation/' + id,
            type: 'DELETE',
            dataType: 'json',
            contentType: 'application/json'
        });
        setTimeout((function () {
        }), 1000);
        $(this.content).empty();
        return $.ajax({
            url: 'http://localhost:8080/airline/source/reservation',
            type: 'GET',
            dataType: 'json'
        }).done(this.printList);
    };

    ReservationList.prototype.updateData = function (e) {
        this.id = document.getElementById(e.target.id).getAttribute("data-reservation-id");
        $(this.content).empty();
        return $.ajax({
            url: 'http://localhost:8080/airline/source/reservation/' + this.id,
            type: 'GET',
            dataType: 'json',
            headers: {'x-password': prompt("Password")}
        }).done(this.createForm);
    };

    ReservationList.prototype.createForm = function (data) {
        var form, table, tbody;
        form = document.createElement("form");
        form.method = "post";
        form.action = "#";
        table = document.createElement("table");
        tbody = document.createElement("tbody");
        tbody.innerHTML = this.getForm(data);
        table.appendChild(tbody);
        form.appendChild(table);
        this.content.appendChild(form);
        //$('#dateOfDeparture').datetimepicker({format:pickerformat});
        return $('form').submit(this.sendForm);
    };

    ReservationList.prototype.sendForm = function (e) {
        var ar;
        e.preventDefault();
        ar = $(e.target).serializeArray();
        this.data = {};
        this.data[ar[0].name] = ar[0].value;
        this.data[ar[1].name] = ar[1].value;
        this.data[ar[2].name] = ar[2].value;
        this.data[ar[3].name] = Number(ar[3].value);
        this.data["url"] = "";
        this.data["state"] = "NEW";
        $.ajax({
            url: 'http://localhost:8080/airline/source/reservation/' + this.id,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data),
            headers: {'x-password': prompt("Password")}
        });
        return $(this.content).empty();
    };

    ReservationList.prototype.getForm = function (data) {
        return "<tr> <th>Flight: </th> <td><input type=\"text\" name=\"flight\" id=\"flight\" value=\"" + data.flight + "\"></td> </tr> <tr> <th>Created: </th> <td><input type=\"text\" name=\"created\" id=\"created\" value=\"" + data.created + "\"></td> </tr> <tr> <th>Password: </th> <td><input type=\"text\" name=\"password\" id=\"password\" value=\"" + data.password + "\"></td> </tr> <tr> <th>Seats: </th> <td><input type=\"number\" name=\"seats\" id=\"seats\" value=\"" + data.seats + "\"></td> </tr> <tr> <th></th> <td><input type=\"submit\" value=\"Update\"></td> </tr>";
    };

    return ReservationList;

})();


OnlineClients = function () {

    start = function () {
        ws = new WebSocket("ws://" + document.location.hostname + ":" + document.location.port + "/airline/endpoint");
        
        setTimeout(function () {
            ws.onmessage = function (evt) {
                jQuery("#onlineClients").text("Počet klientů:" + evt.data);
            };
        }, 2000);

        setInterval(repeat = function () {
            ws.send("count");//zeptat se websocket na pocet klientu
        }, 2000);

    };

    return {start: start};
};

client = new Client(document.getElementById("container"), document.getElementById("content"));
client.run();