<% if((request.getHeader("Content-type") != null) && (!request.getHeader("Content-type").equals("text/html"))) { %>
[{"name":"flight","uri":"/flight/"},{"name":"destination","uri":"/destination/"},{"name":"reservation","uri":"/reservation/"}]
<% } else {%>
<%@page contentType="text/html" pageEncoding="UTF-8"  import="java.io.*,java.util.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Client</title>
        <link rel="stylesheet" href="/airline/style.css">
        <link rel="stylesheet" href="/airline/jquery.datetimepicker.css">       
    </head>
    <body>
        <div id="container">
            <nav>
                <ul>
                    <li>
                        Flight: <a id="createFlight" href="#createFlight">create</a>
                        <a id="listFlights" href="#listFlights">list</a>
                    </li>
                    <li>
                        Destination:
                        <a id="createDestination" href="#createDestination">create</a>
                        <a id="listDestinations" href="#listDestinations">list</a>
                    </li>
                    <li>
                        Reservation:
                        <a id="createReservation" href="#createReservation">create</a>
                        <a id="listReservations" href="#listReservations">list</a>
                    </li>
                    <li>
                        Payment:
                        <a id="makePayment" href="#makePayment">create</a>
                    </li>
                </ul>
            </nav>
            <div id="content"></div>
            <div id="onlineClients"></div>
        </div>
        <script src="/airline/jquery-2.1.1.min.js"></script>
        <script src="/airline/script.js"></script>         
        <script src="/airline/jquery.datetimepicker.js"></script>
    </body>
</html>
<% }%>   